<?php

namespace core;

class Controller
{
    public $session;
    public $language;
    public $view;
    public $db;

    function __construct()
    {
        $this->session = $_SESSION;
        $this->view = new View();
        $this->language = $this->addLanguage();
    }

    function index()
    {

    }

    //проверка принятых данных
    public function preparation($post)
    {
        $post = stripslashes($post);
        $post = htmlspecialchars(trim($post));
        $post = str_replace(["`", "%", "../", "\0"], "", $post);

        return $post;
    }

    //добавление языка
    public function addLanguage()
    {
        if (isset($this->session['language'])) {
            return $this->session['language'];
        } else {
            //по дефолту язык русский
            $_SESSION['language'] = 'ru';

            return 'ru';
        }
    }


}
