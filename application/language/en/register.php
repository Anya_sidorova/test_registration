<?php

return [
    'error_email' => 'Email field is required',
    'error_email_auth' => 'Email field is required',
    'error_email_cor' => 'Email field is invalid',
    'error_password' => 'Password field is required',
    'error_password_notcor' => 'Passwords do not match',
    'error_password_auth' => 'Password field is required',
    'error_first_name' => 'Password Firsrname is required',
    'error_last_name' => 'Password Lastname is required',
    'errors' => 'This user is already registered',

    //error_img    
    'error_img_type' => 'Picture must be format .png, .jpeg, .jpg, .gif',
    'error_img' => 'Error loading image',
    'error_img_mime' => 'Only images can be uploaded',
    'error_img_max_size' => 'Upload file size exceeds value MAX_FILE_SIZE',
    'error_img_no_upload' => 'File failed to load',
];
