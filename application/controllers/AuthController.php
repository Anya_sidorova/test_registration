<?php

namespace application\controllers;

use application\models\User;

use core\Controller;

class AuthController extends Controller
{

    private $errors;
    private $variables_language;

    public function __construct($language_path)
    {
        parent::__construct();
        $this->variables_language = include($language_path);
    }

    public function actionAuth()
    {
        $user_model = new User();

        if (isset($_POST)) {

            $email = $this->preparation($_POST['email']);
            $password = $this->preparation($_POST['password']);

            if ($user_model->isEmpty($email)) {
                $this->errors[] = $this->variables_language['error_email_auth'];
            }

            if ($user_model->isEmpty($password)) {
                $this->errors[] = $this->variables_language['error_password_auth'];
            }

            if (!isset($this->errors)) {
                $userId = $user_model->checkUserData($email, $password);
                if ($userId) {
                    $user_info = $user_model->getUser($userId);
                    $_SESSION['user'] = $user_info;
                } else {
                    $this->errors[] = $this->variables_language['errors'];
                }
            }
            echo json_encode($this->errors);
        }
    }
}
